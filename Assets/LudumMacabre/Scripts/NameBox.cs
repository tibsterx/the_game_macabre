﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Fungus;

public class NameBox : MonoBehaviour
{
    public Text nameText;
    public Text DisplayText;
    public Flowchart flowChart;
    public Character playerCharacter;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(nameText.text.ToString() != "") {
            if (Input.GetKeyDown(KeyCode.Return)) {
                flowChart.ExecuteBlock("R1Q1-1");
                DisplayText.text = flowChart.GetStringVariable("PlayerName");
            }
        }
    }
}
