﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Fungus;

public class ChangeScore : MonoBehaviour
{
    public Text[] scores = new Text[4];
    public Flowchart[] flowCharts = new Flowchart[3];

    public void UpdateScoresAct1() {
        scores[0].text = flowCharts[0].GetIntegerVariable("PlayerPoints").ToString();
        scores[1].text = flowCharts[0].GetIntegerVariable("StrongPoints").ToString();
        scores[2].text = flowCharts[0].GetIntegerVariable("InnocentPoints").ToString();
        scores[3].text = flowCharts[0].GetIntegerVariable("LadyPoints").ToString();
    }

    public void UpdateScoresAct2() {
        scores[0].text = flowCharts[1].GetIntegerVariable("PlayerPoints2").ToString();
        scores[1].text = flowCharts[1].GetIntegerVariable("StrongPoints2").ToString();
        scores[2].text = flowCharts[1].GetIntegerVariable("InnocentPoints2").ToString();
        scores[3].text = flowCharts[1].GetIntegerVariable("LadyPoints2").ToString();
    }
    public void UpdateScoresAct3() {
        scores[0].text = flowCharts[2].GetIntegerVariable("PlayerPoints3").ToString();
        scores[1].text = flowCharts[2].GetIntegerVariable("StrongPoints3").ToString();
        scores[2].text = flowCharts[2].GetIntegerVariable("InnocentPoints3").ToString();
        scores[3].text = flowCharts[2].GetIntegerVariable("LadyPoints3").ToString();
    }
}
