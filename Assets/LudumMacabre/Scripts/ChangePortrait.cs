﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class ChangePortrait : MonoBehaviour
{
    public Character characterPlayer;
    public GameObject inGamePlayer;
    public GameObject arenaPlayer;
    public Sprite[] portraits = new Sprite[6];

    public void SetPortrait0() {
        Flowchart[] flowcharts = FindObjectsOfType<Flowchart> ();
        foreach (var flowyChart in flowcharts) {
            if (flowyChart != null) {
                foreach (var sayCommand in flowyChart.GetComponentsInChildren<Say>()) {
                    if (sayCommand._Character == characterPlayer) {
                        sayCommand.Portrait = portraits[0];
                    }
                }
            }
        }

        inGamePlayer.GetComponent<SpriteRenderer>().sprite = portraits[0]; 
        arenaPlayer.GetComponent<SpriteRenderer>().sprite = portraits[0];
    }

    public void SetPortrait1() {
        Flowchart[] flowcharts = FindObjectsOfType<Flowchart>();
        foreach (var flowyChart in flowcharts) {
            if (flowyChart != null) {
                foreach (var sayCommand in flowyChart.GetComponentsInChildren<Say>()) {
                    if (sayCommand._Character == characterPlayer) {
                        sayCommand.Portrait = portraits[1];
                    }
                }
            }
        }

        inGamePlayer.GetComponent<SpriteRenderer>().sprite = portraits[1];
        arenaPlayer.GetComponent<SpriteRenderer>().sprite = portraits[1];
    }

    public void SetPortrait2() {
        Flowchart[] flowcharts = FindObjectsOfType<Flowchart>();
        foreach (var flowyChart in flowcharts) {
            if (flowyChart != null) {
                foreach (var sayCommand in flowyChart.GetComponentsInChildren<Say>()) {
                    if (sayCommand._Character == characterPlayer) {
                        sayCommand.Portrait = portraits[2];
                    }
                }
            }
        }

        inGamePlayer.GetComponent<SpriteRenderer>().sprite = portraits[2];
        arenaPlayer.GetComponent<SpriteRenderer>().sprite = portraits[2];
    }

    public void SetPortrait3() {
        Flowchart[] flowcharts = FindObjectsOfType<Flowchart>();
        foreach (var flowyChart in flowcharts) {
            if (flowyChart != null) {
                foreach (var sayCommand in flowyChart.GetComponentsInChildren<Say>()) {
                    if (sayCommand._Character == characterPlayer) {
                        sayCommand.Portrait = portraits[3];
                    }
                }
            }
        }

        inGamePlayer.GetComponent<SpriteRenderer>().sprite = portraits[3];
        arenaPlayer.GetComponent<SpriteRenderer>().sprite = portraits[3];
    }

    public void SetPortrait4() {
        Flowchart[] flowcharts = FindObjectsOfType<Flowchart>();
        foreach (var flowyChart in flowcharts) {
            if (flowyChart != null) {
                foreach (var sayCommand in flowyChart.GetComponentsInChildren<Say>()) {
                    if (sayCommand._Character == characterPlayer) {
                        sayCommand.Portrait = portraits[4];
                    }
                }
            }
        }

        inGamePlayer.GetComponent<SpriteRenderer>().sprite = portraits[4];
        arenaPlayer.GetComponent<SpriteRenderer>().sprite = portraits[4];
    }

    public void SetPortrait5() {
        Flowchart[] flowcharts = FindObjectsOfType<Flowchart>();
        foreach (var flowyChart in flowcharts) {
            if (flowyChart != null) {
                foreach (var sayCommand in flowyChart.GetComponentsInChildren<Say>()) {
                    if (sayCommand._Character == characterPlayer) {
                        sayCommand.Portrait = portraits[5];
                    }
                }
            }
        }

        inGamePlayer.GetComponent<SpriteRenderer>().sprite = portraits[5];
        arenaPlayer.GetComponent<SpriteRenderer>().sprite = portraits[5];
    }

}
